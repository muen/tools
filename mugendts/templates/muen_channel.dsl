__cchannel_bus_alias____cchannel_bus_name__@__cchannel_bus_base__ {
    compatible = "muen,communication-channel";
    __cchannel_registers__
    interrupt-parent = <0x7>;
    interrupts = <GIC_SPI 0x__cchannel_irq_irq__ IRQ_TYPE_LEVEL_HIGH>;
    type = <__cchannel_type__>;
    status = "okay";
};
