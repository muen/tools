--D @Interface
--D This package contains constant definitions for hardware-dependent I/O
--D devices operated by the kernel. The values are derived from the system
--D policy.
package Skp.Hardware
  with
    SPARK_Mode => On
is

__device_resources__
end Skp.Hardware;
